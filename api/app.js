'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

// cargar rutas

var user_routes = require('./routes/user');

// middlewares ( metodo que se ejecuta antes del controlador )

// Convertir a JSON el body
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// cors

//rutas

app.use('/api', user_routes);

// exportar conf

module.exports = app;